#!/usr/bin/env perl

# (C) 2005, Artem Khodush <greenkaa@gmail.com>
# (C) 2020, Kyle J. McKay <mackyle@gmail.com>
# All rights reserved.

# Parts originally from gitweb.cgi
# (C) 2005-2006, Kay Sievers <kay.sievers@vrfy.org>
# (C) 2005, Christian Gierke <ch@gierke.de>

# This program is licensed under the GPL v2

use 5.008;
use strict;
no warnings;
use CGI qw(:standard :escapeHTML -nosticky);
use CGI::Util qw(unescape);
#use CGI::Carp qw(fatalsToBrowser);

binmode STDOUT, ':utf8';

my $cgi=CGI::new();

package git::inner;

use Encode;
use File::Spec;
use vars qw($gitdir $cgi $urlbase $action $hash $hash_parent);

# location of the git-core binaries
$git::inner::gitbin="git";
# location of the gitweb URL
$git::inner::gitweb="/gitweb.cgi";
# include blame links?
$git::inner::blame_links=1;
$git::inner::blame_action='blame_incremental';

# rename detection options for git-diff-tree
# - default is '-M', with the cost proportional to
#   (number of removed files) * (number of new files).
# - more costly is '-C' (which implies '-M'), with the cost proportional to
#   (number of changed files + number of removed files) * (number of new files)
# - even more costly is '-C', '--find-copies-harder' with cost
#   (number of files in the original tree) * (number of new files)
# - one might want to include '-B' option, e.g. '-B', '-M'
@git::inner::diff_opts = ('-B', '-C');

# opens a "-|" cmd pipe handle with 2>/dev/null and returns it
sub cmd_pipe {
	open(NULL, '>', File::Spec->devnull) or die "Cannot open devnull: $!\n";
	open(SAVEERR, ">&STDERR") || die "couldn't dup STDERR: $!\n";
	open(STDERR, ">&NULL") || die "couldn't dup NULL to STDERR: $!\n";
	my $result = open(my $fd, "-|", @_);
	open(STDERR, ">&SAVEERR") || die "couldn't dup SAVERR to STDERR: $!\n";
	close(SAVEERR) or die "couldn't close SAVEERR: $!\n";
	close(NULL) or die "couldn't close NULL: $!\n";
	return $result ? $fd : undef;
}

# opens a "-|" git_cmd pipe handle with 2>/dev/null and returns it
# returns undef and sets a non-zero $! if the pipe is empty and
# the command failed (otherwise, if the command succeeded and the
# pipe is empty, a read-only handle on devnull is returned).
sub git_cmd_pipe {
	my $p = cmd_pipe $git::inner::gitbin, "--git-dir=".$gitdir, @_;
	defined($p) or return undef;
	eof($p) or return $p;
	close($p);
	my $e = $?;
	$e = ($e >= 256) ? ($e > 32768 ? 255 : $e >> 8) : ($e & 0x7f) + 128 if $e;
	$e and $! = $e, return undef;
	undef $p;
	return open($p, '<', File::Spec->devnull) ? $p : undef;
}

my $fallback_encoding;
BEGIN {
	$fallback_encoding = Encode::find_encoding('Windows-1252');
	$fallback_encoding = Encode::find_encoding('ISO-8859-1')
		unless $fallback_encoding;
}

# decode sequences of octets in utf8 into Perl's internal form,
# which is utf-8 with utf8 flag set if needed.  git-diff writes out
# in utf-8 thanks to "binmode STDOUT, ':utf8'" at beginning
sub to_utf8 {
	my $str = shift || '';
	if (Encode::is_utf8($str) || utf8::decode($str)) {
		return $str;
	} else {
		return $fallback_encoding->decode($str, Encode::FB_DEFAULT);
	}
}

# much simpler than including all of Fcntl :mode since Git's modes are fixed
sub S_ISREG {($_[0] & 0170000) == 0100000}

# Git only knows about these modes:
#   040000 - directory
#   100644 - plain file
#   100755 - executable file
#   120000 - symbolic link
#   160000 - submodule
sub file_type {
	my $m = shift;
	defined($m) or return "";
	$m =~ /^[0-7]+$/os or return $m;
	my $mx = oct($m);
	my $ft = ($mx & 0170000) >> 12;
	# in rough order of expected decreasing probability
	$ft == 010 and return ($mx & 0111) ? "executable" : "file";
	$ft == 004 and return "directory";
	$ft == 012 and return "symlink";
	$ft == 016 and return "submodule";
	return "unknown";
}

# always correct version of $cgi->a({-href = href("-anchor", "...")}, "...")
sub a_anchor {
	my ($opts, $text) = @_;
	my $href = CGI::escapeHTML($opts->{"-href"});
	return "<a href=\"$href\">".CGI::escapeHTML($text)."</a>";
}

# a much simpler version than the one from gitweb
#   -anchor makes a fragment link, everything else is ignored
# otherwise any combination of the following are allowed:
#   action -> 'a' parameter
#   hash -> 'h' parameter
#   hash_base -> 'hb' parameter
#   hash_parent -> 'hp' parameter
#   hash_parent_base -> 'hpb' parameter
#   file_name -> 'f' parameter
#   file_parent -> 'fp' parameter
my %pmap;
BEGIN { %pmap = (
	action => '0a',
	hash => '1h',
	hash_base => '2hb',
	hash_parent => '3hp',
	hash_parent_base => '4hpb',
	file_name => '5f',
	file_parent => '6fp'
) }
sub href {
	my %opts = @_;
	if (exists($opts{"-anchor"})) {
		return '#'.esc_param($opts{"-anchor"});
	}
	my @params = ();
	foreach my $k (keys %opts) {
		if (exists($pmap{$k})) {
			my $p = $pmap{$k};
			push(@params, $p.'='.esc_param($opts{$k}));
		}
	}
	@params = map(substr($_,1), sort @params);
	return @params ? $urlbase.'?'.join('&',@params) : "";
}

#
## BEGIN gitweb.cgi source
#

# quote unsafe chars, but keep the slash, even when it's not
# correct, but quoted slashes look too horrible in bookmarks
sub esc_param {
	my $str = shift;
	return undef unless defined $str;
	$str =~ s/([^A-Za-z0-9\-_.~()\/:@ ]+)/CGI::escape($1)/eg;
	$str =~ s/ /\+/g;
	return $str;
}

# replace invalid utf8 character with SUBSTITUTION sequence
sub esc_html {
	my $str = shift;
	my %opts = @_;

	return undef unless defined $str;

	$str = to_utf8($str);
	$str = $cgi->escapeHTML($str);
	if ($opts{'-nbsp'}) {
		$str =~ s/ /&#160;/g;
	}
	use bytes;
	$str =~ s|([[:cntrl:]])|(($1 ne "\t") ? quot_cec($1) : $1)|eg;
	return $str;
}

# quote control characters and escape filename to HTML
sub esc_path {
	my $str = shift;
	my %opts = @_;

	return undef unless defined $str;

	$str = to_utf8($str);
	$str = $cgi->escapeHTML($str);
	if ($opts{'-nbsp'}) {
		$str =~ s/ /&#160;/g;
	}
	use bytes;
	$str =~ s|([[:cntrl:]])|quot_cec($1)|eg;
	return $str;
}

# Make control characters "printable", using character escape codes (CEC)
sub quot_cec {
	my $cntrl = shift;
	my %opts = @_;
	my %es = ( # character escape codes, aka escape sequences
		"\t" => '\t',   # tab            (HT)
		"\n" => '\n',   # line feed      (LF)
		"\r" => '\r',   # carrige return (CR)
		"\f" => '\f',   # form feed      (FF)
		"\b" => '\b',   # backspace      (BS)
		"\a" => '\a',   # alarm (bell)   (BEL)
		"\e" => '\e',   # escape         (ESC)
		"\013" => '\v', # vertical tab   (VT)
		"\000" => '\0', # nul character  (NUL)
	);
	my $chr = ( (exists $es{$cntrl})
		    ? $es{$cntrl}
		    : sprintf('\x%02x', ord($cntrl)) );
	if ($opts{-nohtml}) {
		return $chr;
	} else {
		return "<span class=\"cntrl\">$chr</span>";
	}
}

# git may return quoted and escaped filenames
sub unquote {
	my $str = shift;

	sub unq {
		my $seq = shift;
		my %es = ( # character escape codes, aka escape sequences
			't' => "\t",   # tab            (HT, TAB)
			'n' => "\n",   # newline        (NL)
			'r' => "\r",   # return         (CR)
			'f' => "\f",   # form feed      (FF)
			'b' => "\b",   # backspace      (BS)
			'a' => "\a",   # alarm (bell)   (BEL)
			'e' => "\e",   # escape         (ESC)
			'v' => "\013", # vertical tab   (VT)
		);

		if ($seq =~ m/^[0-7]{1,3}$/) {
			# octal char sequence
			return chr(oct($seq));
		} elsif (exists $es{$seq}) {
			# C escape sequence, aka character escape code
			return $es{$seq};
		}
		# quoted ordinary character
		return $seq;
	}

	if ($str =~ m/^"(.*)"$/) {
		# needs unquoting
		$str = $1;
		$str =~ s/\\([^0-7]|[0-7]{1,3})/unq($1)/eg;
	}
	return $str;
}

# escape tabs (convert tabs to spaces)
# improved faster version from Markdown.pl
sub untabify {
	my $line = shift;
	# From the Perl camel book section "Fluent Perl" but modified a bit
        $line =~ s/(.*?)(\t+)/$1 . ' ' x (length($2) * 8 - length($1) % 8)/ges;
	return $line;
}

# Highlight selected fragments of string, using given CSS class,
# and escape HTML.  It is assumed that fragments do not overlap.
# Regions are passed as list of pairs (array references).
#
# Example: esc_html_hl_regions("foobar", "mark", [ 0, 3 ]) returns
# '<span class="mark">foo</span>bar'
sub esc_html_hl_regions {
	my ($str, $css_class, @sel) = @_;
	my %opts = grep { ref($_) ne 'ARRAY' } @sel;
	@sel     = grep { ref($_) eq 'ARRAY' } @sel;
	return esc_html($str, %opts) unless @sel;

	my $out = '';
	my $pos = 0;

	for my $s (@sel) {
		my ($begin, $end) = @$s;

		# Don't create empty <span> elements.
		next if $end <= $begin;

		my $escaped = esc_html(substr($str, $begin, $end - $begin),
		                       %opts);

		$out .= esc_html(substr($str, $pos, $begin - $pos), %opts)
			if ($begin - $pos > 0);
		$out .= $cgi->span({-class => $css_class}, $escaped);

		$pos = $end;
	}
	$out .= esc_html(substr($str, $pos), %opts)
		if ($pos < length($str));

	return $out;
}

# format git diff header line, i.e. "diff --(git|combined|cc) ..."
sub format_git_diff_header_line {
	my $line = shift;
	my $diffinfo = shift;
	my ($from, $to) = @_;

	if ($diffinfo->{'nparents'}) {
		# combined diff
		$line =~ s!^(diff (.*?) )"?.*$!$1!;
		if ($to->{'href'}) {
			$line .= $cgi->a({-href => $to->{'href'}, -class => "path"},
			                 esc_path($to->{'file'}));
		} else { # file was deleted (no href)
			$line .= esc_path($to->{'file'});
		}
	} else {
		# "ordinary" diff
		$line =~ s!^(diff (.*?) )"?a/.*$!$1!;
		if ($from->{'href'}) {
			$line .= $cgi->a({-href => $from->{'href'}, -class => "path"},
			                 'a/' . esc_path($from->{'file'}));
		} else { # file was added (no href)
			$line .= 'a/' . esc_path($from->{'file'});
		}
		$line .= ' ';
		if ($to->{'href'}) {
			$line .= $cgi->a({-href => $to->{'href'}, -class => "path"},
			                 'b/' . esc_path($to->{'file'}));
		} else { # file was deleted
			$line .= 'b/' . esc_path($to->{'file'});
		}
	}

	return "<div class=\"diff header\">$line</div>\n";
}

# format extended diff header line, before patch itself
sub format_extended_diff_header_line {
	my $line = shift;
	my $diffinfo = shift;
	my ($from, $to) = @_;

	# match <path>
	if ($line =~ s!^((copy|rename) from ).*$!$1! && $from->{'href'}) {
		$line .= $cgi->a({-href=>$from->{'href'}, -class=>"path"},
		                       esc_path($from->{'file'}));
	}
	if ($line =~ s!^((copy|rename) to ).*$!$1! && $to->{'href'}) {
		$line .= $cgi->a({-href=>$to->{'href'}, -class=>"path"},
		                 esc_path($to->{'file'}));
	}
	# match single <mode>
	if ($line =~ m/\s(\d{6})$/) {
		$line .= '<span class="info"> (' .
		         file_type($1) .
		         ')</span>';
	}
	# match <hash>
	if ($line =~ m/^index [0-9a-fA-F]{40},[0-9a-fA-F]{40}/) {
		# can match only for combined diff
		$line = 'index ';
		for (my $i = 0; $i < $diffinfo->{'nparents'}; $i++) {
			if ($from->{'href'}[$i]) {
				$line .= $cgi->a({-href=>$from->{'href'}[$i],
				                  -class=>"hash"},
				                 substr($diffinfo->{'from_id'}[$i],0,7));
			} else {
				$line .= '0' x 7;
			}
			# separator
			$line .= ',' if ($i < $diffinfo->{'nparents'} - 1);
		}
		$line .= '..';
		if ($to->{'href'}) {
			$line .= $cgi->a({-href=>$to->{'href'}, -class=>"hash"},
			                 substr($diffinfo->{'to_id'},0,7));
		} else {
			$line .= '0' x 7;
		}

	} elsif ($line =~ m/^index [0-9a-fA-F]{40}..[0-9a-fA-F]{40}/) {
		# can match only for ordinary diff
		my ($from_link, $to_link);
		if ($from->{'href'}) {
			$from_link = $cgi->a({-href=>$from->{'href'}, -class=>"hash"},
			                     substr($diffinfo->{'from_id'},0,7));
		} else {
			$from_link = '0' x 7;
		}
		if ($to->{'href'}) {
			$to_link = $cgi->a({-href=>$to->{'href'}, -class=>"hash"},
			                   substr($diffinfo->{'to_id'},0,7));
		} else {
			$to_link = '0' x 7;
		}
		my ($from_id, $to_id) = ($diffinfo->{'from_id'}, $diffinfo->{'to_id'});
		$line =~ s!$from_id\.\.$to_id!$from_link..$to_link!;
	}

	return $line . "<br/>\n";
}

# format from-file/to-file diff header
sub format_diff_from_to_header {
	my ($from_line, $to_line, $diffinfo, $from, $to, @parents) = @_;
	my $line;
	my $result = '';

	$line = $from_line;
	#assert($line =~ m/^---/) if DEBUG;
	# no extra formatting for "^--- /dev/null"
	if (! $diffinfo->{'nparents'}) {
		# ordinary (single parent) diff
		if ($line =~ m!^--- "?a/!) {
			if ($from->{'href'}) {
				$line = '--- a/' .
				        $cgi->a({-href=>$from->{'href'}, -class=>"path"},
				                esc_path($from->{'file'}));
			} else {
				$line = '--- a/' .
				        esc_path($from->{'file'});
			}
		}
		$result .= qq!<div class="diff from_file">$line</div>\n!;

	} else {
		# combined diff (merge commit)
		for (my $i = 0; $i < $diffinfo->{'nparents'}; $i++) {
			if ($from->{'href'}[$i]) {
				$line = '--- ' .
				        $cgi->a({-href=>href(action=>"blobdiff",
				                             hash_parent=>$diffinfo->{'from_id'}[$i],
				                             hash_parent_base=>$parents[$i],
				                             file_parent=>$from->{'file'}[$i],
				                             hash=>$diffinfo->{'to_id'},
				                             hash_base=>$hash,
				                             file_name=>$to->{'file'}),
				                 -class=>"path",
				                 -title=>"diff" . ($i+1)},
				                $i+1) .
				        '/' .
				        $cgi->a({-href=>$from->{'href'}[$i], -class=>"path"},
				                esc_path($from->{'file'}[$i]));
			} else {
				$line = '--- /dev/null';
			}
			$result .= qq!<div class="diff from_file">$line</div>\n!;
		}
	}

	$line = $to_line;
	#assert($line =~ m/^\+\+\+/) if DEBUG;
	# no extra formatting for "^+++ /dev/null"
	if ($line =~ m!^\+\+\+ "?b/!) {
		if ($to->{'href'}) {
			$line = '+++ b/' .
			        $cgi->a({-href=>$to->{'href'}, -class=>"path"},
			                esc_path($to->{'file'}));
		} else {
			$line = '+++ b/' .
			        esc_path($to->{'file'});
		}
	}
	$result .= qq!<div class="diff to_file">$line</div>\n!;

	return $result;
}

# create note for patch simplified by combined diff
sub format_diff_cc_simplified {
	my ($diffinfo, @parents) = @_;
	my $result = '';

	$result .= "<div class=\"diff header\">" .
	           "diff --cc ";
	if (!is_deleted($diffinfo)) {
		$result .= $cgi->a({-href => href(action=>"blob",
		                                  hash_base=>$hash,
		                                  hash=>$diffinfo->{'to_id'},
		                                  file_name=>$diffinfo->{'to_file'}),
		                    -class => "path"},
		                   esc_path($diffinfo->{'to_file'}));
	} else {
		$result .= esc_path($diffinfo->{'to_file'});
	}
	$result .= "</div>\n" . # class="diff header"
	           "<div class=\"diff nodifferences\">" .
	           "Simple merge" .
	           "</div>\n"; # class="diff nodifferences"

	return $result;
}

sub diff_line_class {
	my ($line, $from, $to) = @_;

	# ordinary diff
	my $num_sign = 1;
	# combined diff
	if ($from && $to && ref($from->{'href'}) eq "ARRAY") {
		$num_sign = scalar @{$from->{'href'}};
	}

	my @diff_line_classifier = (
		{ regexp => qr/^\@\@{$num_sign} /, class => "chunk_header"},
		{ regexp => qr/^\\/,               class => "incomplete"  },
		{ regexp => qr/^ {$num_sign}/,     class => "ctx" },
		# classifier for context must come before classifier add/rem,
		# or we would have to use more complicated regexp, for example
		# qr/(?= {0,$m}\+)[+ ]{$num_sign}/, where $m = $num_sign - 1;
		{ regexp => qr/^[+ ]{$num_sign}/,   class => "add" },
		{ regexp => qr/^[- ]{$num_sign}/,   class => "rem" },
	);
	for my $clsfy (@diff_line_classifier) {
		return $clsfy->{'class'}
			if ($line =~ $clsfy->{'regexp'});
	}

	# fallback
	return "";
}

# assumes that $from and $to are defined and correctly filled,
# and that $line holds a line of chunk header for unified diff
sub format_unidiff_chunk_header {
	my ($line, $from, $to) = @_;

	my ($from_text, $from_start, $from_lines, $to_text, $to_start, $to_lines, $section) =
		$line =~ m/^\@{2} (-(\d+)(?:,(\d+))?) (\+(\d+)(?:,(\d+))?) \@{2}(.*)$/;

	$from_lines = 0 unless defined $from_lines;
	$to_lines   = 0 unless defined $to_lines;

	if ($from->{'href'}) {
		$from_text = $cgi->a({-href=>"$from->{'href'}#l$from_start",
		                     -class=>"list"}, $from_text);
	}
	if ($to->{'href'}) {
		$to_text   = $cgi->a({-href=>"$to->{'href'}#l$to_start",
		                     -class=>"list"}, $to_text);
	}
	$line = "<span class=\"chunk_info\">@@ $from_text $to_text @@</span>" .
	        "<span class=\"section\">" . esc_html($section, -nbsp=>1) . "</span>";
	return $line;
}

# assumes that $from and $to are defined and correctly filled,
# and that $line holds a line of chunk header for combined diff
sub format_cc_diff_chunk_header {
	my ($line, $from, $to) = @_;

	my ($prefix, $ranges, $section) = $line =~ m/^(\@+) (.*?) \@+(.*)$/;
	my (@from_text, @from_start, @from_nlines, $to_text, $to_start, $to_nlines);

	@from_text = split(' ', $ranges);
	for (my $i = 0; $i < @from_text; ++$i) {
		($from_start[$i], $from_nlines[$i]) =
			(split(',', substr($from_text[$i], 1)), 0);
	}

	$to_text   = pop @from_text;
	$to_start  = pop @from_start;
	$to_nlines = pop @from_nlines;

	$line = "<span class=\"chunk_info\">$prefix ";
	for (my $i = 0; $i < @from_text; ++$i) {
		if ($from->{'href'}[$i]) {
			$line .= $cgi->a({-href=>"$from->{'href'}[$i]#l$from_start[$i]",
			                  -class=>"list"}, $from_text[$i]);
		} else {
			$line .= $from_text[$i];
		}
		$line .= " ";
	}
	if ($to->{'href'}) {
		$line .= $cgi->a({-href=>"$to->{'href'}#l$to_start",
		                  -class=>"list"}, $to_text);
	} else {
		$line .= $to_text;
	}
	$line .= " $prefix</span>" .
	         "<span class=\"section\">" . esc_html($section, -nbsp=>1) . "</span>";
	return $line;
}

# process patch (diff) line (not to be used for diff headers),
# returning HTML-formatted (but not wrapped) line.
# If the line is passed as a reference, it is treated as HTML and not
# esc_html()'ed.
sub format_diff_line {
	my ($line, $diff_class, $from, $to) = @_;

	if (ref($line)) {
		$line = $$line;
	} else {
		chomp $line;
		$line = untabify($line);

		if ($from && $to && $line =~ m/^\@{2} /) {
			$line = format_unidiff_chunk_header($line, $from, $to);
		} elsif ($from && $to && $line =~ m/^\@{3}/) {
			$line = format_cc_diff_chunk_header($line, $from, $to);
		} else {
			$line = esc_html($line, -nbsp=>1);
		}
	}

	my $diff_classes = "diff diff_body";
	$diff_classes .= " $diff_class" if ($diff_class);
	$line = "<div class=\"$diff_classes\">$line</div>\n";

	return $line;
}

# get path of entry with given hash at given tree-ish (ref)
# used to get 'from' filename for combined diff (merge commit) for renames
sub git_get_path_by_hash {
	my $base = shift || return;
	my $hash = shift || return;

	local $/ = "\0";

	defined(my $fd = git_cmd_pipe "ls-tree", '-r', '-t', '-z', $base)
		or return undef;
	while (my $line = to_utf8(scalar <$fd>)) {
		chomp $line;

		#'040000 tree 595596a6a9117ddba9fe379b6b012b558bac8423	gitweb'
		#'100644 blob e02e90f0429be0d2a69b76571101f20b8f75530f	gitweb/README'
		if ($line =~ m/(?:[0-9]+) (?:.+) $hash\t(.+)$/) {
			close $fd;
			return $1;
		}
	}
	close $fd;
	return undef;
}

# parse line of git-diff-tree "raw" output
sub parse_difftree_raw_line {
	my $line = shift;
	my %res;

	# ':100644 100644 03b218260e99b78c6df0ed378e59ed9205ccc96d 3b93d5e7cc7f7dd4ebed13a5cc1a4ad976fc94d8 M	ls-files.c'
	# ':100644 100644 7f9281985086971d3877aca27704f2aaf9c448ce bc190ebc71bbd923f2b728e505408f5e54bd073a M	rev-tree.c'
	if ($line =~ m/^:([0-7]{6}) ([0-7]{6}) ([0-9a-fA-F]{40}) ([0-9a-fA-F]{40}) (.)([0-9]{0,3})\t(.*)$/) {
		$res{'from_mode'} = $1;
		$res{'to_mode'} = $2;
		$res{'from_id'} = $3;
		$res{'to_id'} = $4;
		$res{'status'} = $5;
		$res{'similarity'} = $6;
		if ($res{'status'} eq 'R' || $res{'status'} eq 'C') { # renamed or copied
			($res{'from_file'}, $res{'to_file'}) = map { unquote($_) } split("\t", $7);
		} else {
			$res{'from_file'} = $res{'to_file'} = $res{'file'} = unquote($7);
		}
	}
	# '::100755 100755 100755 60e79ca1b01bc8b057abe17ddab484699a7f5fdb 94067cc5f73388f33722d52ae02f44692bc07490 94067cc5f73388f33722d52ae02f44692bc07490 MR	git-gui/git-gui.sh'
	# combined diff (for merge commit)
	elsif ($line =~ s/^(::+)((?:[0-7]{6} )+)((?:[0-9a-fA-F]{40} )+)([a-zA-Z]+)\t(.*)$//) {
		$res{'nparents'}  = length($1);
		$res{'from_mode'} = [ split(' ', $2) ];
		$res{'to_mode'} = pop @{$res{'from_mode'}};
		$res{'from_id'} = [ split(' ', $3) ];
		$res{'to_id'} = pop @{$res{'from_id'}};
		$res{'status'} = [ split('', $4) ];
		$res{'to_file'} = unquote($5);
	}
	# 'c512b523472485aef4fff9e57b229d9d243c967f'
	elsif ($line =~ m/^([0-9a-fA-F]{40})$/) {
		$res{'commit'} = $1;
	}

	return wantarray ? %res : \%res;
}

# wrapper: return parsed line of git-diff-tree "raw" output
# (the argument might be raw line, or parsed info)
sub parsed_difftree_line {
	my $line_or_ref = shift;

	if (ref($line_or_ref) eq "HASH") {
		# pre-parsed (or generated by hand)
		return $line_or_ref;
	} else {
		return parse_difftree_raw_line($line_or_ref);
	}
}

# generates _two_ hashes, references to which are passed as 2 and 3 argument
sub parse_from_to_diffinfo {
	my ($diffinfo, $from, $to, @parents) = @_;

	if ($diffinfo->{'nparents'}) {
		# combined diff
		$from->{'file'} = [];
		$from->{'href'} = [];
		fill_from_file_info($diffinfo, @parents)
			unless exists $diffinfo->{'from_file'};
		for (my $i = 0; $i < $diffinfo->{'nparents'}; $i++) {
			$from->{'file'}[$i] =
				defined $diffinfo->{'from_file'}[$i] ?
				        $diffinfo->{'from_file'}[$i] :
				        $diffinfo->{'to_file'};
			if ($diffinfo->{'status'}[$i] ne "A") { # not new (added) file
				$from->{'href'}[$i] = href(action=>"blob",
				                           hash_base=>$parents[$i],
				                           hash=>$diffinfo->{'from_id'}[$i],
				                           file_name=>$from->{'file'}[$i]);
			} else {
				$from->{'href'}[$i] = undef;
			}
		}
	} else {
		# ordinary (not combined) diff
		$from->{'file'} = $diffinfo->{'from_file'};
		if ($diffinfo->{'status'} ne "A") { # not new (added) file
			$from->{'href'} = href(action=>"blob", hash_base=>$hash_parent,
			                       hash=>$diffinfo->{'from_id'},
			                       file_name=>$from->{'file'});
		} else {
			delete $from->{'href'};
		}
	}

	$to->{'file'} = $diffinfo->{'to_file'};
	if (!is_deleted($diffinfo)) { # file exists in result
		$to->{'href'} = href(action=>"blob", hash_base=>$hash,
		                     hash=>$diffinfo->{'to_id'},
		                     file_name=>$to->{'file'});
	} else {
		delete $to->{'href'};
	}
}

# get pre-image filenames for merge (combined) diff
sub fill_from_file_info {
	my ($diff, @parents) = @_;

	$diff->{'from_file'} = [ ];
	$diff->{'from_file'}[$diff->{'nparents'} - 1] = undef;
	for (my $i = 0; $i < $diff->{'nparents'}; $i++) {
		if ($diff->{'status'}[$i] eq 'R' ||
		    $diff->{'status'}[$i] eq 'C') {
			$diff->{'from_file'}[$i] =
				git_get_path_by_hash($parents[$i], $diff->{'from_id'}[$i]);
		}
	}

	return $diff;
}

# is current raw difftree line of file deletion
sub is_deleted {
	my $diffinfo = shift;

	return $diffinfo->{'to_id'} =~ /^0{40,}$/os;
}

# does patch correspond to [previous] difftree raw line
# $diffinfo  - hashref of parsed raw diff format
# $patchinfo - hashref of parsed patch diff format
#              (the same keys as in $diffinfo)
sub is_patch_split {
	my ($diffinfo, $patchinfo) = @_;

	return defined $diffinfo && defined $patchinfo
		&& $diffinfo->{'to_file'} eq $patchinfo->{'to_file'};
}

sub git_difftree_body {
	my ($difftree, $hash, @parents) = @_;
	my ($parent) = $parents[0];
	my $have_blame = $git::inner::blame_links;
	if ($#{$difftree} > 10) {
		print "<div class=\"list_head\">\n";
		print(($#{$difftree} + 1) . " files changed:\n");
		print "</div>\n";
	}

	print "<table class=\"" .
	      (@parents > 1 ? "combined " : "") .
	      "diff_tree\">\n";

	# header only for combined diff in 'commitdiff' view
	my $has_header = @$difftree && @parents > 1 && $action eq 'commitdiff';
	if ($has_header) {
		# table header
		print "<thead><tr>\n" .
		       "<th></th><th></th>\n"; # filename, patchN link
		for (my $i = 0; $i < @parents; $i++) {
			my $par = $parents[$i];
			print "<th>" .
			      $cgi->a({-href => href(action=>"commitdiff",
			                             hash=>$hash, hash_parent=>$par),
			               -title => 'commitdiff to parent number ' .
			                          ($i+1) . ': ' . substr($par,0,7)},
			              $i+1) .
			      "&#160;</th>\n";
		}
		print "</tr></thead>\n<tbody>\n";
	}

	my $alternate = 1;
	my $patchno = 0;
	foreach my $line (@{$difftree}) {
		my $diff = parsed_difftree_line($line);

		if ($alternate) {
			print "<tr class=\"dark\">\n";
		} else {
			print "<tr class=\"light\">\n";
		}
		$alternate ^= 1;

		if (exists $diff->{'nparents'}) { # combined diff

			fill_from_file_info($diff, @parents)
				unless exists $diff->{'from_file'};

			if (!is_deleted($diff)) {
				# file exists in the result (child) commit
				print "<td>" .
				      $cgi->a({-href => href(action=>"blob", hash=>$diff->{'to_id'},
				                             file_name=>$diff->{'to_file'},
				                             hash_base=>$hash),
				              -class => "list"}, esc_path($diff->{'to_file'})) .
				      "</td>\n";
			} else {
				print "<td>" .
				      esc_path($diff->{'to_file'}) .
				      "</td>\n";
			}

			if ($action eq 'commitdiff') {
				# link to patch
				$patchno++;
				print "<td class=\"link\">" .
				      a_anchor({-href => href(-anchor=>"patch$patchno")},
				              "patch") .
				      " | " .
				      "</td>\n";
			}

			my $has_history = 0;
			my $not_deleted = 0;
			for (my $i = 0; $i < $diff->{'nparents'}; $i++) {
				my $hash_parent = $parents[$i];
				my $from_hash = $diff->{'from_id'}[$i];
				my $from_path = $diff->{'from_file'}[$i];
				my $status = $diff->{'status'}[$i];

				$has_history ||= ($status ne 'A');
				$not_deleted ||= ($status ne 'D');

				if ($status eq 'A') {
					print "<td  class=\"link\" align=\"right\"> | </td>\n";
				} elsif ($status eq 'D') {
					print "<td class=\"link\">" .
					      $cgi->a({-href => href(action=>"blob",
					                             hash_base=>$hash,
					                             hash=>$from_hash,
					                             file_name=>$from_path)},
					              "blob" . ($i+1)) .
					      " | </td>\n";
				} else {
					if ($diff->{'to_id'} eq $from_hash) {
						print "<td class=\"link nochange\">";
					} else {
						print "<td class=\"link\">";
					}
					print $cgi->a({-href => href(action=>"blobdiff",
					                             hash=>$diff->{'to_id'},
					                             hash_parent=>$from_hash,
					                             hash_base=>$hash,
					                             hash_parent_base=>$hash_parent,
					                             file_name=>$diff->{'to_file'},
					                             file_parent=>$from_path)},
					              "diff" . ($i+1)) .
					      " | </td>\n";
				}
			}

			print "<td class=\"link\">";
			if ($not_deleted) {
				print $cgi->a({-href => href(action=>"blob",
				                             hash=>$diff->{'to_id'},
				                             file_name=>$diff->{'to_file'},
				                             hash_base=>$hash)},
				              "blob");
				print " | " if ($has_history);
			}
			if ($has_history) {
				print $cgi->a({-href => href(action=>"history",
				                             file_name=>$diff->{'to_file'},
				                             hash_base=>$hash)},
				              "history");
			}
			print "</td>\n";

			print "</tr>\n";
			next; # instead of 'else' clause, to avoid extra indent
		}
		# else ordinary diff

		my ($to_mode_oct, $to_mode_str, $to_file_type);
		my ($from_mode_oct, $from_mode_str, $from_file_type);
		if ($diff->{'to_mode'} ne ('0' x 6)) {
			$to_mode_oct = oct $diff->{'to_mode'};
			if (S_ISREG($to_mode_oct)) { # only for regular file
				$to_mode_str = sprintf("%04o", $to_mode_oct & 0777); # permission bits
			}
			$to_file_type = file_type($diff->{'to_mode'});
		}
		if ($diff->{'from_mode'} ne ('0' x 6)) {
			$from_mode_oct = oct $diff->{'from_mode'};
			if (S_ISREG($from_mode_oct)) { # only for regular file
				$from_mode_str = sprintf("%04o", $from_mode_oct & 0777); # permission bits
			}
			$from_file_type = file_type($diff->{'from_mode'});
		}

		if ($diff->{'status'} eq "A") { # created
			my $mode_chng = "<span class=\"file_status new\">[new $to_file_type";
			$mode_chng   .= " with mode: $to_mode_str" if $to_mode_str;
			$mode_chng   .= "]</span>";
			print "<td>";
			print $cgi->a({-href => href(action=>"blob", hash=>$diff->{'to_id'},
			                             hash_base=>$hash, file_name=>$diff->{'file'}),
			              -class => "list"}, esc_path($diff->{'file'}));
			print "</td>\n";
			print "<td>$mode_chng</td>\n";
			print "<td class=\"link\">";
			if ($action eq 'commitdiff') {
				# link to patch
				$patchno++;
				print a_anchor({-href => href(-anchor=>"patch$patchno")},
				              "patch") .
				      " | ";
			}
			print $cgi->a({-href => href(action=>"blob", hash=>$diff->{'to_id'},
			                             hash_base=>$hash, file_name=>$diff->{'file'})},
			              "blob");
			print "</td>\n";

		} elsif ($diff->{'status'} eq "D") { # deleted
			my $mode_chng = "<span class=\"file_status deleted\">[deleted $from_file_type]</span>";
			print "<td>";
			print $cgi->a({-href => href(action=>"blob", hash=>$diff->{'from_id'},
			                             hash_base=>$parent, file_name=>$diff->{'file'}),
			               -class => "list"}, esc_path($diff->{'file'}));
			print "</td>\n";
			print "<td>$mode_chng</td>\n";
			print "<td class=\"link\">";
			if ($action eq 'commitdiff') {
				# link to patch
				$patchno++;
				print a_anchor({-href => href(-anchor=>"patch$patchno")},
				              "patch") .
				      " | ";
			}
			print $cgi->a({-href => href(action=>"blob", hash=>$diff->{'from_id'},
			                             hash_base=>$parent, file_name=>$diff->{'file'})},
			              "blob") . " | ";
			if ($have_blame) {
				print $cgi->a({-href => href(action=>$git::inner::blame_action, hash_base=>$parent,
							     file_name=>$diff->{'file'}),
					      -class => "blamelink"},
				              "blame") . " | ";
			}
			print $cgi->a({-href => href(action=>"history", hash_base=>$parent,
			                             file_name=>$diff->{'file'})},
			              "history");
			print "</td>\n";

		} elsif ($diff->{'status'} eq "M" || $diff->{'status'} eq "T") { # modified, or type changed
			my $mode_chnge = "";
			if ($diff->{'from_mode'} != $diff->{'to_mode'}) {
				$mode_chnge = "<span class=\"file_status mode_chnge\">[changed";
				if ($from_file_type ne $to_file_type) {
					$mode_chnge .= " from $from_file_type to $to_file_type";
				}
				if (($from_mode_oct & 0777) != ($to_mode_oct & 0777)) {
					if ($from_mode_str && $to_mode_str) {
						$mode_chnge .= " mode: $from_mode_str->$to_mode_str";
					} elsif ($to_mode_str) {
						$mode_chnge .= " mode: $to_mode_str";
					}
				}
				$mode_chnge .= "]</span>\n";
			}
			print "<td>";
			print $cgi->a({-href => href(action=>"blob", hash=>$diff->{'to_id'},
			                             hash_base=>$hash, file_name=>$diff->{'file'}),
			              -class => "list"}, esc_path($diff->{'file'}));
			print "</td>\n";
			print "<td>$mode_chnge</td>\n";
			print "<td class=\"link\">";
			if ($action eq 'commitdiff') {
				# link to patch
				$patchno++;
				print a_anchor({-href => href(-anchor=>"patch$patchno")},
				              "patch") .
				      " | ";
			} elsif ($diff->{'to_id'} ne $diff->{'from_id'}) {
				# "commit" view and modified file (not onlu mode changed)
				print $cgi->a({-href => href(action=>"blobdiff",
				                             hash=>$diff->{'to_id'}, hash_parent=>$diff->{'from_id'},
				                             hash_base=>$hash, hash_parent_base=>$parent,
				                             file_name=>$diff->{'file'})},
				              "diff") .
				      " | ";
			}
			print $cgi->a({-href => href(action=>"blob", hash=>$diff->{'to_id'},
			                             hash_base=>$hash, file_name=>$diff->{'file'})},
			               "blob") . " | ";
			if ($have_blame) {
				print $cgi->a({-href => href(action=>$git::inner::blame_action, hash_base=>$hash,
							     file_name=>$diff->{'file'}),
					      -class => "blamelink"},
				              "blame") . " | ";
			}
			print $cgi->a({-href => href(action=>"history", hash_base=>$hash,
			                             file_name=>$diff->{'file'})},
			              "history");
			print "</td>\n";

		} elsif ($diff->{'status'} eq "R" || $diff->{'status'} eq "C") { # renamed or copied
			my %status_name = ('R' => 'moved', 'C' => 'copied');
			my $nstatus = $status_name{$diff->{'status'}};
			my $mode_chng = "";
			if ($diff->{'from_mode'} != $diff->{'to_mode'}) {
				# mode also for directories, so we cannot use $to_mode_str
				$mode_chng = sprintf(", mode: %04o", $to_mode_oct & 0777);
			}
			print "<td>" .
			      $cgi->a({-href => href(action=>"blob", hash_base=>$hash,
			                             hash=>$diff->{'to_id'}, file_name=>$diff->{'to_file'}),
			              -class => "list"}, esc_path($diff->{'to_file'})) . "</td>\n" .
			      "<td><span class=\"file_status $nstatus\">[$nstatus from " .
			      $cgi->a({-href => href(action=>"blob", hash_base=>$parent,
			                             hash=>$diff->{'from_id'}, file_name=>$diff->{'from_file'}),
			              -class => "list"}, esc_path($diff->{'from_file'})) .
			      " with " . (int $diff->{'similarity'}) . "% similarity$mode_chng]</span></td>\n" .
			      "<td class=\"link\">";
			if ($action eq 'commitdiff') {
				# link to patch
				$patchno++;
				print a_anchor({-href => href(-anchor=>"patch$patchno")},
				              "patch") .
				      " | ";
			} elsif ($diff->{'to_id'} ne $diff->{'from_id'}) {
				# "commit" view and modified file (not only pure rename or copy)
				print $cgi->a({-href => href(action=>"blobdiff",
				                             hash=>$diff->{'to_id'}, hash_parent=>$diff->{'from_id'},
				                             hash_base=>$hash, hash_parent_base=>$parent,
				                             file_name=>$diff->{'to_file'}, file_parent=>$diff->{'from_file'})},
				              "diff") .
				      " | ";
			}
			print $cgi->a({-href => href(action=>"blob", hash=>$diff->{'to_id'},
			                             hash_base=>$parent, file_name=>$diff->{'to_file'})},
			              "blob") . " | ";
			if ($have_blame) {
				print $cgi->a({-href => href(action=>$git::inner::blame_action, hash_base=>$hash,
							     file_name=>$diff->{'to_file'}),
					      -class => "blamelink"},
				              "blame") . " | ";
			}
			print $cgi->a({-href => href(action=>"history", hash_base=>$hash,
			                            file_name=>$diff->{'to_file'})},
			              "history");
			print "</td>\n";

		} # we should not encounter Unmerged (U) or Unknown (X) status
		print "</tr>\n";
	}
	print "</tbody>" if $has_header;
	print "</table>\n";
}

# Print context lines and then rem/add lines in a side-by-side manner.
sub print_sidebyside_diff_lines {
	my ($ctx, $rem, $add) = @_;

	# print context block before add/rem block
	if (@$ctx) {
		print join '',
			'<div class="chunk_block ctx">',
				'<div class="old">',
				@$ctx,
				'</div>',
				'<div class="new">',
				@$ctx,
				'</div>',
			'</div>';
	}

	if (!@$add) {
		# pure removal
		print join '',
			'<div class="chunk_block rem">',
				'<div class="old">',
				@$rem,
				'</div>',
			'</div>';
	} elsif (!@$rem) {
		# pure addition
		print join '',
			'<div class="chunk_block add">',
				'<div class="new">',
				@$add,
				'</div>',
			'</div>';
	} else {
		print join '',
			'<div class="chunk_block chg">',
				'<div class="old">',
				@$rem,
				'</div>',
				'<div class="new">',
				@$add,
				'</div>',
			'</div>';
	}
}

# Print context lines and then rem/add lines in inline manner.
sub print_inline_diff_lines {
	my ($ctx, $rem, $add) = @_;

	print @$ctx, @$rem, @$add;
}

# Format removed and added line, mark changed part and HTML-format them.
# Implementation is based on contrib/diff-highlight
sub format_rem_add_lines_pair {
	my ($rem, $add, $num_parents) = @_;

	# We need to untabify lines before split()'ing them;
	# otherwise offsets would be invalid.
	chomp $rem;
	chomp $add;
	$rem = untabify($rem);
	$add = untabify($add);

	my @rem = split(//, $rem);
	my @add = split(//, $add);
	my ($esc_rem, $esc_add);
	# Ignore leading +/- characters for each parent.
	my ($prefix_len, $suffix_len) = ($num_parents, 0);
	my ($prefix_has_nonspace, $suffix_has_nonspace);

	my $shorter = (@rem < @add) ? @rem : @add;
	while ($prefix_len < $shorter) {
		last if ($rem[$prefix_len] ne $add[$prefix_len]);

		$prefix_has_nonspace = 1 if ($rem[$prefix_len] !~ /\s/);
		$prefix_len++;
	}

	while ($prefix_len + $suffix_len < $shorter) {
		last if ($rem[-1 - $suffix_len] ne $add[-1 - $suffix_len]);

		$suffix_has_nonspace = 1 if ($rem[-1 - $suffix_len] !~ /\s/);
		$suffix_len++;
	}

	# Mark lines that are different from each other, but have some common
	# part that isn't whitespace.  If lines are completely different, don't
	# mark them because that would make output unreadable, especially if
	# diff consists of multiple lines.
	if ($prefix_has_nonspace || $suffix_has_nonspace) {
		$esc_rem = esc_html_hl_regions($rem, 'marked',
		        [$prefix_len, @rem - $suffix_len], -nbsp=>1);
		$esc_add = esc_html_hl_regions($add, 'marked',
		        [$prefix_len, @add - $suffix_len], -nbsp=>1);
	} else {
		$esc_rem = esc_html($rem, -nbsp=>1);
		$esc_add = esc_html($add, -nbsp=>1);
	}

	return format_diff_line(\$esc_rem, 'rem'),
	       format_diff_line(\$esc_add, 'add');
}

# HTML-format diff context, removed and added lines.
sub format_ctx_rem_add_lines {
	my ($ctx, $rem, $add, $num_parents) = @_;
	my (@new_ctx, @new_rem, @new_add);
	my $can_highlight = 0;
	my $is_combined = ($num_parents > 1);

	# Highlight if every removed line has a corresponding added line.
	if (@$add > 0 && @$add == @$rem) {
		$can_highlight = 1;

		# Highlight lines in combined diff only if the chunk contains
		# diff between the same version, e.g.
		#
		#    - a
		#   -  b
		#    + c
		#   +  d
		#
		# Otherwise the highlightling would be confusing.
		if ($is_combined) {
			for (my $i = 0; $i < @$add; $i++) {
				my $prefix_rem = substr($rem->[$i], 0, $num_parents);
				my $prefix_add = substr($add->[$i], 0, $num_parents);

				$prefix_rem =~ s/-/+/g;

				if ($prefix_rem ne $prefix_add) {
					$can_highlight = 0;
					last;
				}
			}
		}
	}

	if ($can_highlight) {
		for (my $i = 0; $i < @$add; $i++) {
			my ($line_rem, $line_add) = format_rem_add_lines_pair(
			        $rem->[$i], $add->[$i], $num_parents);
			push @new_rem, $line_rem;
			push @new_add, $line_add;
		}
	} else {
		@new_rem = map { format_diff_line($_, 'rem') } @$rem;
		@new_add = map { format_diff_line($_, 'add') } @$add;
	}

	@new_ctx = map { format_diff_line($_, 'ctx') } @$ctx;

	return (\@new_ctx, \@new_rem, \@new_add);
}

# Print context lines and then rem/add lines.
sub print_diff_lines {
	my ($ctx, $rem, $add, $diff_style, $num_parents) = @_;
	my $is_combined = $num_parents > 1;

	($ctx, $rem, $add) = format_ctx_rem_add_lines($ctx, $rem, $add,
	        $num_parents);

	if ($diff_style eq 'sidebyside' && !$is_combined) {
		print_sidebyside_diff_lines($ctx, $rem, $add);
	} else {
		# default 'inline' style and unknown styles
		print_inline_diff_lines($ctx, $rem, $add);
	}
}

sub print_diff_chunk {
	my ($diff_style, $num_parents, $from, $to, @chunk) = @_;
	my (@ctx, @rem, @add);

	# The class of the previous line.
	my $prev_class = '';

	return unless @chunk;

	# incomplete last line might be among removed or added lines,
	# or both, or among context lines: find which
	for (my $i = 1; $i < @chunk; $i++) {
		if ($chunk[$i][0] eq 'incomplete') {
			$chunk[$i][0] = $chunk[$i-1][0];
		}
	}

	# guardian
	push @chunk, ["", ""];

	foreach my $line_info (@chunk) {
		my ($class, $line) = @$line_info;

		# print chunk headers
		if ($class && $class eq 'chunk_header') {
			print format_diff_line($line, $class, $from, $to);
			next;
		}

		## print from accumulator when have some add/rem lines or end
		# of chunk (flush context lines), or when have add and rem
		# lines and new block is reached (otherwise add/rem lines could
		# be reordered)
		if (!$class || ((@rem || @add) && $class eq 'ctx') ||
		    (@rem && @add && $class ne $prev_class)) {
			print_diff_lines(\@ctx, \@rem, \@add,
		                         $diff_style, $num_parents);
			@ctx = @rem = @add = ();
		}

		## adding lines to accumulator
		# guardian value
		last unless $line;
		# rem, add or change
		if ($class eq 'rem') {
			push @rem, $line;
		} elsif ($class eq 'add') {
			push @add, $line;
		}
		# context line
		if ($class eq 'ctx') {
			push @ctx, $line;
		}

		$prev_class = $class;
	}
}

sub git_patchset_body {
	my ($fd, $diff_style, $difftree, $hash, @hash_parents) = @_;
	my ($hash_parent) = $hash_parents[0];

	my $is_combined = (@hash_parents > 1);
	my $patch_idx = 0;
	my $patch_number = 0;
	my $patch_line;
	my $diffinfo;
	my $to_name;
	my (%from, %to);
	my @chunk; # for side-by-side diff

	print "<div class=\"patchset\">\n";

	# skip to first patch
	while ($patch_line = to_utf8(scalar <$fd>)) {
		chomp $patch_line;

		last if ($patch_line =~ m/^diff /);
	}

 PATCH:
	while ($patch_line) {

		# parse "git diff" header line
		if ($patch_line =~ m/^diff --git (\"(?:[^\\\"]*(?:\\.[^\\\"]*)*)\"|[^ "]*) (.*)$/) {
			# $1 is from_name, which we do not use
			$to_name = unquote($2);
			$to_name =~ s!^b/!!;
		} elsif ($patch_line =~ m/^diff --(cc|combined) ("?.*"?)$/) {
			# $1 is 'cc' or 'combined', which we do not use
			$to_name = unquote($2);
		} else {
			$to_name = undef;
		}

		# check if current patch belong to current raw line
		# and parse raw git-diff line if needed
		if (is_patch_split($diffinfo, { 'to_file' => $to_name })) {
			# this is continuation of a split patch
			print "<div class=\"patch cont\">\n";
		} else {
			# advance raw git-diff output if needed
			$patch_idx++ if defined $diffinfo;

			# read and prepare patch information
			$diffinfo = parsed_difftree_line($difftree->[$patch_idx]);

			# compact combined diff output can have some patches skipped
			# find which patch (using pathname of result) we are at now;
			if ($is_combined) {
				while ($to_name ne $diffinfo->{'to_file'}) {
					print "<div class=\"patch\" id=\"patch". ($patch_idx+1) ."\">\n" .
					      format_diff_cc_simplified($diffinfo, @hash_parents) .
					      "</div>\n";  # class="patch"

					$patch_idx++;
					$patch_number++;

					last if $patch_idx > $#$difftree;
					$diffinfo = parsed_difftree_line($difftree->[$patch_idx]);
				}
			}

			# modifies %from, %to hashes
			parse_from_to_diffinfo($diffinfo, \%from, \%to, @hash_parents);

			# this is first patch for raw difftree line with $patch_idx index
			# we index @$difftree array from 0, but number patches from 1
			print "<div class=\"patch\" id=\"patch". ($patch_idx+1) ."\">\n";
		}

		# git diff header
		#assert($patch_line =~ m/^diff /) if DEBUG;
		#assert($patch_line !~ m!$/$!) if DEBUG; # is chomp-ed
		$patch_number++;
		# print "git diff" header
		print format_git_diff_header_line($patch_line, $diffinfo,
		                                  \%from, \%to);

		# print extended diff header
		print "<div class=\"diff extended_header\">\n";
	EXTENDED_HEADER:
		while ($patch_line = to_utf8(scalar<$fd>)) {
			chomp $patch_line;

			last EXTENDED_HEADER if ($patch_line =~ m/^--- |^diff /);

			print format_extended_diff_header_line($patch_line, $diffinfo,
			                                       \%from, \%to);
		}
		print "</div>\n"; # class="diff extended_header"

		# from-file/to-file diff header
		if (! $patch_line) {
			print "</div>\n"; # class="patch"
			last PATCH;
		}
		next PATCH if ($patch_line =~ m/^diff /);
		#assert($patch_line =~ m/^---/) if DEBUG;

		my $last_patch_line = $patch_line;
		$patch_line = to_utf8(scalar <$fd>);
		chomp $patch_line;
		#assert($patch_line =~ m/^\+\+\+/) if DEBUG;

		print format_diff_from_to_header($last_patch_line, $patch_line,
		                                 $diffinfo, \%from, \%to,
		                                 @hash_parents);

		# the patch itself
	LINE:
		while ($patch_line = to_utf8(scalar <$fd>)) {
			chomp $patch_line;

			next PATCH if ($patch_line =~ m/^diff /);

			my $class = diff_line_class($patch_line, \%from, \%to);

			if ($class eq 'chunk_header') {
				print_diff_chunk($diff_style, scalar @hash_parents, \%from, \%to, @chunk);
				@chunk = ();
			}

			push @chunk, [ $class, $patch_line ];
		}

	} continue {
		if (@chunk) {
			print_diff_chunk($diff_style, scalar @hash_parents, \%from, \%to, @chunk);
			@chunk = ();
		}
		print "</div>\n"; # class="patch"
	}

	# for compact combined (--cc) format, with chunk and patch simplification
	# the patchset might be empty, but there might be unprocessed raw lines
	for (++$patch_idx if $patch_number > 0;
	     $patch_idx < @$difftree;
	     ++$patch_idx) {
		# read and prepare patch information
		$diffinfo = parsed_difftree_line($difftree->[$patch_idx]);

		# generate anchor for "patch" links in difftree / whatchanged part
		print "<div class=\"patch\" id=\"patch". ($patch_idx+1) ."\">\n" .
		      format_diff_cc_simplified($diffinfo, @hash_parents) .
		      "</div>\n";  # class="patch"

		$patch_number++;
	}

	if ($patch_number == 0) {
		if (@hash_parents > 1) {
			print "<div class=\"diff nodifferences\">Trivial merge</div>\n";
		} else {
			print "<div class=\"diff nodifferences\">No differences found</div>\n";
		}
	}

	print "</div>\n"; # class="patchset"
}

#
## END gitweb.cgi source
#

sub git_header_html {
	my $status = shift || "200 OK";
	my $expires = shift;

	print $cgi->header(-type=>'text/html',  -charset => 'utf-8', -status=> $status, -expires => $expires);
	print <<EOF;
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>git diff</title>
</head>
<body>
EOF
}

sub git_footer_html {
	print "</body>\n" .
	      "</html>";
}

sub die_error {
	my $status = shift || "403 Forbidden";
	my $error = shift || "Malformed query, file missing or permission denied";

	git_header_html($status);
	print "<div class=\"page_body\">\n" .
	      "<br /><br />\n" .
	      "$status - $error\n" .
	      "<br />\n" .
	      "</div>\n";
	git_footer_html();
	exit;
}

sub git_commitdiff {
	my ($id1, $id2)=@_;
	local $hash_parent = $id1;
	local $hash = $id2;
	defined(my $fd = git_cmd_pipe 'diff-tree', '-r', @git::inner::diff_opts,
		'--no-commit-id', '--patch-with-raw', '--full-index', $id1, $id2, '--') or
                die_error(undef, "Open git-diff-tree failed: @{[0+$!]}\n");
	my @difftree;
	while (my $line = to_utf8(scalar <$fd>)) {
		chomp $line;
		# empty line ends raw part of diff-tree output
		last unless $line;
		push @difftree, scalar parse_difftree_raw_line($line);
	}
	my $expires = $inner::http_expires;
	if ((!defined($expires) || $expires eq "") &&
		$id1 =~ m/^[0-9a-fA-F]{40,}$/ && $id2 =~ m/^[0-9a-fA-F]{40,}$/) {
		$expires = "+1d";
	}

	git_header_html(undef, $expires);
	print "<div class=\"page_body\">\n";
	git_difftree_body(\@difftree, $hash, $hash_parent);
	#print "<br />\n";
	git_patchset_body($fd, 'inline', \@difftree, $hash, $hash_parent);
	print "</div>";
	git_footer_html();
}

package inner;

# Set the global doconfig setting in the GITBROWSER_CONFIG file to the full
# path to a perl source file to run to alter these settings

# If $check_path is set to a subroutine reference, it will be called
# by get_repo_path with two arguments, the name of the repo and its
# path which will be undef if it's not a known repo.  If the function
# returns false, access to the repo will be denied.
# $check_path = sub { my ($name, $path) = @_; $name ~! /restricted/i; };
use vars qw($check_path);

use Cwd qw(abs_path);
use File::Basename qw(dirname);
use File::Spec::Functions qw(file_name_is_absolute catdir);

sub read_config
{
	my $f;
	my $GITBROWSER_CONFIG = $ENV{'GITBROWSER_CONFIG'} || "git-browser.conf";
	-e $GITBROWSER_CONFIG or $GITBROWSER_CONFIG = "/etc/git-browser.conf";

	open $f, '<', $GITBROWSER_CONFIG or return;
	my $confdir = dirname(abs_path($GITBROWSER_CONFIG));
	my $section="";
	my $configfile="";
	while( <$f> ) {
		chomp;
		$_=~ s/\r$//;
		if( $section eq "repos" ) {
			if( m/^\w+:\s*/ ) {
				$section="";
				redo;
			}else {
				my ($name,$path)=split;
				if( $name && $path ) {
					file_name_is_absolute($path) or
						$path = catdir($confdir, $path);
					$inner::known_repos{$name}=$path;
				}
			}
		}else {
			if( m/^gitbin:\s*/ ) {
				$git::inner::gitbin=$';
			}elsif( m/^gitweb:\s*/ ) {
				$git::inner::gitweb=$';
			}elsif( m/^path:\s*/ ) {
				$ENV{PATH}=$';
			}elsif( m/^http_expires:\s*/ ) {
				$inner::http_expires=$';
			}elsif( m/^warehouse:\s*/ ) {
				my $path = $';
				file_name_is_absolute($path) or
					$path = catdir($confdir, $path);
				$inner::warehouse=$path;
			}elsif( m/^doconfig:\s*/ ) {
				$configfile=$';
			}elsif( m/^repos:\s*/ ) {
				$section="repos";
			}
		}
	}
	if ($configfile && -e $configfile) {
		do $configfile;
		die $@ if $@;
	}
	$git::inner::gitweb .= "/" unless substr($git::inner::gitweb, -1, 1) eq "/";
}

package main;

use File::Spec::Functions qw(catdir);

sub get_repo_path
{
	my ($name) = @_;
	my $path = $inner::known_repos{$name};
	return undef
	    if ref($inner::check_path) eq 'CODE' && !&{$inner::check_path}($name, $path);
	if (not defined $path and $inner::warehouse and -d catdir($inner::warehouse, $name)) {
		$path = catdir($inner::warehouse, $name);
	}
	return $path;
}

sub validate_input {
	my $input = shift;

	if ($input =~ m/^[0-9a-fA-F]{40,}$/) {
		return $input;
	}
	if ($input =~ m/(?:^|\/)(?:|\.|\.\.)(?:$|\/)/) {
		return undef;
	}
	if ($input =~ m/[^a-zA-Z0-9_\x80-\xff\ \t\.\/\-\+\*\~\%\,\x21-\x7e]/) {
		return undef;
	}
	return $input;
}

inner::read_config();

my $repo=$cgi->param( "repo" );
my $id1=$cgi->param( "id1" );
my $id2=$cgi->param( "id2" );

git::inner::die_error( "403 Forbidden", "malformed value for repo param" )
	unless defined( validate_input( $repo ) ) && get_repo_path( $repo );
git::inner::die_error( "403 Forbidden", "malformed value for id1 param" ) unless defined validate_input( $id1 );
git::inner::die_error( "403 Forbidden", "malformed value for id2 param" ) unless defined validate_input( $id2 );

{
	my $encrepo = $repo;
	{
		use bytes;
		$encrepo =~ s/([\x00-\x1F\x7F-\xFF <>"#%{}|\\^`?])/sprintf("%%%02X",ord($1))/gse;
	}
	local $git::inner::gitdir = get_repo_path($repo);
	local $git::inner::cgi = $cgi;
	local $git::inner::urlbase = $git::inner::gitweb . $encrepo;
	local $git::inner::action = 'commitdiff';

	git::inner::git_commitdiff( $id1, $id2 );
}
